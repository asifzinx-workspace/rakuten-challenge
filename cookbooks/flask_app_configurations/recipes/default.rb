#
# Cookbook:: flask_app_configurations
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.


pip_requirements '/vagrant/deploy/requirements.txt'


# bash 'install_requirements_packages' do
#   code <<-EOH
#     cd /vagrant/deploy/
# 	pip install -r requirements.txt 
#     EOH
#   user "vagrant"
#   #environment 'HOME' => "/home/#{user}"
#   only_if { ::File.exist?('/vagrant/deploy/requirements.txt') }
# end


template '/etc/init/flask.conf' do
  source "asifzinx_flask.erb"
end


template '/lib/systemd/system/flask.service' do
  source "asifzinx_flask_service.erb"
end


file '/vagrant/deploy/app.py' do
  mode '0777'
  owner 'vagrant'
end


bash 'install_requirements_packages' do
  code <<-EOH
		sudo service flask start
		sudo service flask status
    EOH
end
